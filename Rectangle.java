package ru.unn.classes_and_figures;

public class Rectangle extends Figure {
    private int shir;
    private int dlin;
    public Rectangle(int X,int Y,int shirina, int dlina){
        super(X, Y);
        dlin =dlina;
        shir =shirina;
    }

    @Override
    public void square(){
        System.out.println();
        System.out.print("Прямоугольник "+ shir * dlin);
    }
}